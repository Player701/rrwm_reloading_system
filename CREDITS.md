# RRWM reloading system -- Credits

RRWM reloading system is based off of the gameplay mod [RRWM](https://bitbucket.org/Player701/rrwm/). Please see the [RRWM credits file](https://bitbucket.org/Player701/rrwm/src/1.3.0a/CREDITS.md) for information about the origin of the assets used for the example weapons provided in this package.
