//===========================================================================
//
// ExampleHud
//
// A bare-bones ZScript-based HUD demonstrating how to display clip amount
// for reloadable weapons.
//
//===========================================================================
class ExampleHud : BaseStatusBar
{
    private HUDFont mHUDFont;

    //===========================================================================
    //
    // ExampleHud::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    override void Init()
    {
        Super.Init();

        Font fnt = "HUDFONT_DOOM";
        mHUDFont = HUDFont.Create(fnt, fnt.GetCharWidth("0"), Mono_CellLeft, 1, 1);
    }

    //===========================================================================
    //
    // ExampleHud::Draw
    //
    // Draws the HUD.
    //
    //===========================================================================
    override void Draw(int state, double TicFrac)
    {
        Super.Draw(state, TicFrac);

        if (state == HUD_Fullscreen)
        {
            BeginHUD();
            DrawFullscreen();
        }
    }

    //===========================================================================
    //
    // ExampleHud::DrawFullscreen
    //
    // Draws the fullscreen HUD. Note that this version only includes
    // the ammo and clip counters for brevity.
    //
    //===========================================================================
    private void DrawFullscreen()
    {
        Inventory ammotype1, ammotype2;
        [ammotype1, ammotype2] = GetCurrentAmmo();
        int invY = -20;

        // Ammo1
        if (ammotype1 != null)
        {
            DrawInventoryIcon(ammotype1, (-14, -4));
            DrawString(mHUDFont, FormatNumber(ammotype1.Amount, 3), (-30, -20), DI_TEXT_ALIGN_RIGHT);
            invY -= 20;
        }

        // Ammo2
        if (ammotype2 != null && ammotype2 != ammotype1)
        {
            DrawInventoryIcon(ammotype2, (-14, invY + 17));
            DrawString(mHUDFont, FormatNumber(ammotype2.Amount, 3), (-30, invY), DI_TEXT_ALIGN_RIGHT);
            invY -= 20;
        }

        // Clip
        let rel = ReloadableWeapon(CPlayer.ReadyWeapon);

        // First, make sure the player's weapon is reloadable.
        if (rel != null)
        {
            // And if it is, draw the clip amount by calling GetClipAmount.
            DrawString(mHUDFont, FormatNumber(rel.GetClipAmount(), 3), (-30, invY), DI_TEXT_ALIGN_RIGHT);
            invY -= 20;
        }
    }
}
