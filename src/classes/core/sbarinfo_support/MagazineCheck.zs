//===========================================================================
//
// MagazineCheck
//
// A special inventory item to facilitate the display
// of a ReloadableWeapon's clip amount via SBARINFO.
//
//===========================================================================
class MagazineCheck : SbarinfoSupportItem
{
    //===========================================================================
    //
    // MagazineCheck::Tick
    //
    // Sets amount to 0 or 1 depending on whether the player is using
    // a reloadable weapon. Additionally, sets the amount of the MagazineCounter
    // inventory item to be the same as the weapon's clip amount.
    //
    //===========================================================================
    override void Tick()
    {
        Super.Tick();

        if (Owner == null || Owner.player ==  null)
        {
            return;
        }

        let rel = ReloadableWeapon(Owner.player.ReadyWeapon);

        if (rel != null)
        {
            Amount = 1;
            SetMagazineCounter(rel.GetClipAmount());
        }
        else
        {
            Amount = 0;
        }
    }

    //===========================================================================
    //
    // MagazineCheck::SetMagazineCounter
    //
    // Sets the amount of the MagazineCounter inventory item to be equal
    // to the provided amount.
    //
    //===========================================================================
    private void SetMagazineCounter(int amount)
    {
        let mag = Inventory(Owner.FindInventory('MagazineCounter'));

        if (mag == null)
        {
            mag = Inventory(Spawn('MagazineCounter'));

            if (mag == null || !mag.CallTryPickup(Owner))
            {
                return;
            }
        }

        mag.Amount = amount;
    }
}
