//===========================================================================
//
// SbarinfoSupportPlayer
//
// A type of DoomPlayer with a special item in their default inventory
// to support display of the clip amount in SBARINFO HUDs.
//
//===========================================================================
class SbarinfoSupportPlayer : DoomPlayer
{
    Default
    {
        Player.StartItem 'Pistol';
        Player.StartItem 'Clip', 50;

        Player.StartItem 'ZenGun';
        Player.StartItem 'ZenClip', 80;
        Player.StartItem 'DukeShotgun';
        Player.StartItem 'DukeShell', 16;

        Player.StartItem 'MagazineCheck';
    }
}
