//===========================================================================
//
// SbarinfoSupportItem
//
// Base class for special inventory items to make SBARINFO HUDs
// compatible with reloadable weapons.
//
//===========================================================================
class SbarinfoSupportItem : Inventory abstract
{
    Default
    {
        +INVENTORY.UNCLEARABLE;
        +INVENTORY.KEEPDEPLETED;
    }
}
