//===========================================================================
//
// ReloadableWeapon
//
// Defines logic for reloadable weapons.
//
//===========================================================================
class ReloadableWeapon : Weapon abstract
{
    //How much time until the weapon auto-reloads once in its ready state.
    const AUTORELOAD_DELAY = 10;

    // Enumerates available reloading modes.
    enum EReloadMode
    {
        // Full auto: the weapon will reload automatically if its clip is empty,
        // without the need to press the reload button.
        RM_FULLAUTO = 0,

        // Semi-auto: the weapon will reload if attempting to fire it with an empty clip.
        RM_SEMIAUTO = 1,

        // Manual: the weapon will only reload if the reload buttom is pressed.
        RM_MANUAL   = 2
    };

    //Enumerates possible fire state types.
    //Used for convenience instead of bool flags.
    enum EFireType
    {
        //Normal fire.
        FT_Normal,
        //Refire (hold).
        FT_Hold,
        //Fire from an interrupted reloading sequence.
        FT_FromReload
    };

    //Enumerates possible dry-fire state types.
    //Used for convenience instead of bool flags.
    enum EDryFireType
    {
        //Normal dry-fire.
        DFT_Normal,
        //Dry hold (holding fire button while in dry fire).
        DFT_Hold,
        //Cooldown (holding fire button straight after normal fire).
        DFT_Cooldown
    };

    //Enumerates possible next states
    //this weapon can transition to from PlayerPawn.FireWeapon
    enum EAttackState
    {
        //Normal fire.
        AS_NormalFire,
        //Dry fire (or hold).
        AS_DryFireOrHold,
        //Dry fire (straight after normal fire, with the fire button still held).
        AS_DryFireCooldown,
        //Reload
        AS_Reload
    };

    //Enumerates the states of reloading sequence.
    //This is used only when reloading round by round to handle interrupts
    //(manual request by pressing reload button or simply firing the weapon).
    enum EReloadSequenceState
    {
        //Not reloading.
        RSS_None,
        //At least one round has been reloaded.
        RSS_InProgress,
        //Accepting interrupt requests from the user.
        RSS_InProgressInterruptible,
        //An interrupt request from the user has been received.
        RSS_Interrupted,
        //Reloaded and waiting to release reload button.
        RSS_Finished
    };

    //Used in A_JumpIfClipAmount
    enum EJumpCondition
    {
        JC_Equal,
        JC_LessThan,
        JC_GreaterThan,
        JC_LessThanOrEqual,
        JC_GreaterThanOrEqual
    };

    // Defines this weapon's clip capacity.
    private int _clipCapacity;

    //The weapon stores clip information here.
    private WeaponClip _clip;
    // The value remembered by calling SetClipAmmoGive.
    private int _clipAmmoGive;
    // Stores the amount of ticks left before this weapon will automatically reload
    // (if automatic reloading is enabled).
    private int _autoReloadTimer;
    // Stores the current reload sequence state.
    private EReloadSequenceState _reloadSequenceState;
    // Determines which state the weapon will transition to from GetAtkState.
    private EAttackState _nextAttackState;

    property ClipCapacity: _clipCapacity;

    // Enable the SpecialHandlePickup method to prevent auto-switching
    // to a non-empty weapon.
    mixin HandlePickupHook;

    Default
    {
        // This is a kind of a hack.
        // It effectively disables normal sv_weapondrop handling
        // by (mis)directing it to another branch,
        // allowing us to implement our own weapon drop logic.
        // (We're talking about dropping weapons on death here)
        // The main problem is that DropInventory prevents dropping
        // items that have +INVENTORY.UNDROPPABLE / UNTOSSABLE,
        // but the normal sv_weapondrop handling doesn't care (bug?).
        // This creates a need to disable this handling completely
        // because otherwise it would drop a +UNDROPPABLE weapon
        // if our own logic failed to do so.
        DropItem 'None';
    }

    //===========================================================================
    //
    // ReloadableWeapon::GetReloadMode
    //
    // Returns the current reloading mode.
    // Implement your own logic here or simply return a constant value.
    //
    //===========================================================================
    private EReloadMode GetReloadMode()
    {
        return RM_FULLAUTO;
    }

    //===========================================================================
    //
    // ReloadableWeapon::GetAutoReloadThreshold
    //
    // Returns the minimum amount of ammo in reserve required for automatic
    // reloading to start. The amount is expressed as a % of the clip capacity.
    // Implement your own logic here or simply return a constant value.
    //
    //===========================================================================
    private int GetAutoReloadThreshold()
    {
        return 0;
    }

    //===========================================================================
    //
    // ReloadableWeapon::A_ReloadableWeaponReady
    //
    // Handles the WeaponReady logic.
    //
    //===========================================================================
    action(Weapon) state A_ReloadableWeaponReady(int flags = 0)
    {
        // Do not propagate WRF_ALLOWRELOAD flag as we handle it ourselves
        flags &= ~WRF_ALLOWRELOAD;

        // Are we in a reloading sequence?
        bool isInReloadSequence = invoker.IsInReloadSequence(true);

        if (isInReloadSequence)
        {
            // No zoom or user-defined features are allowed while reloading.
            flags &= ~(WRF_ALLOWZOOM | WRF_ALLOWUSER1 | WRF_ALLOWUSER2 | WRF_ALLOWUSER3 | WRF_ALLOWUSER4);

            // NB: cannot fire and/or alt-fire if no ammo
            // (DryFire from reloading is not supported)
            bool canFire, canAltFire;
            [canFire, canAltFire] = invoker.CanFireInReloadSequence();

            if (!canFire)
            {
                flags |= WRF_NOPRIMARY;
            }

            if (!canAltFire)
            {
                flags |= WRF_NOSECONDARY;
            }
        }
        else if (player.PendingWeapon == WP_NOCHANGE && invoker.CheckForReloadRequest())
        {
            // If not in a reloading sequence AND no request to change weapons,
            // go to reload state if there is a request to reload.
            return invoker.BeginReload();
        }

        // Do the WeaponReady stuff.
        A_WeaponReady(flags);
        return null;
    }

    //===========================================================================
    //
    // ReloadableWeapon::A_ReloadFullClip
    //
    // Reloads the entirety of this weapon's clip.
    //
    //===========================================================================
    action(Weapon) void A_ReloadFullClip()
    {
        invoker.ReloadFullClip();
    }

    //===========================================================================
    //
    // ReloadableWeapon::A_ReloadOneRound
    //
    // Reloads one round of this weapon's clip.
    //
    //===========================================================================
    action(Weapon) void A_ReloadOneRound()
    {
        invoker.ReloadOneRound();
    }

    //===========================================================================
    //
    // ReloadableWeapon::A_CheckReloadDone
    //
    // Checks if the weapon is fully loaded and, if so,
    // jumps to the state defined by GetReloadDoneState.
    //
    //===========================================================================
    action(Weapon) state A_CheckReloadDone()
    {
        if (!invoker.CheckReloadDone())
        {
            return null;
        }

        let result = invoker.GetReloadDoneState();

        if (result == null)
        {
            result = invoker.GetReadyState();
        }

        return result;
    }

    //===========================================================================
    //
    // ReloadableWeapon::A_DryReFire
    //
    // Calls A_ReFire if the weapon is currently in a dry-fire state.
    //
    //===========================================================================
    action(Weapon) void A_DryReFire()
    {
        let attackState = invoker._nextAttackState;

        if (attackState == AS_DryFireOrHold || attackState == AS_DryFireCooldown)
        {
            A_ReFire();
        }
    }

    //===========================================================================
    //
    // ReloadableWeapon::A_JumpIfClipAmount
    //
    // Jumps to the specified state label if the current clip amount
    // matches the condition provided. Used in DECORATE only,
    // in ZScript please use A_JumpIf with invoker.GetClipAmount().
    //
    //===========================================================================
    action(Weapon) state A_JumpIfClipAmount(EJumpCondition cond, int amt, statelabel label)
    {
        return A_JumpIf(invoker.CheckJumpCondition(invoker.GetClipAmount(), cond, amt), label);
    }

    //===========================================================================
    //
    // ReloadableWeapon::A_JumpIfFullClip
    //
    // Jumps to the specified state label if the clip is full. DECORATE only.
    // In ZScript please use A_JumpIf with invoker.GetClipAmount().
    //
    //===========================================================================
    action(Weapon) state A_JumpIfFullClip(statelabel label)
    {
        return A_JumpIf(invoker.IsFullClip(), label);
    }

    //===========================================================================
    //
    // ReloadableWeapon::CheckJumpCondition
    //
    // Tests the provided actual value against the specified condition
    // and the provided expected value.
    //
    //===========================================================================
    private bool CheckJumpCondition(int actual, EJumpCondition cond, int expected)
    {
        switch (cond)
        {
            case JC_Equal:
                return actual == expected;
            case JC_LessThan:
                return actual < expected;
            case JC_GreaterThan:
                return actual > expected;
            case JC_LessThanOrEqual:
                return actual <= expected;
            case JC_GreaterThanOrEqual:
                return actual >= expected;
        }

        ThrowAbortException("%s: Unknown jump condition value (%d).", GetClassName(), cond);
        return false;
    }

    //===========================================================================
    //
    // ReloadableWeapon::Tick
    //
    // Handles the request to interrupt reloading.
    //
    //===========================================================================
    override void Tick()
    {
        Super.Tick();

        if (Owner == null || Owner.player == null || Owner.player.ReadyWeapon != self)
        {
            return;
        }

        bool reloadBtn = IsReloadButtonPressed();

        if (_reloadSequenceState == RSS_InProgress && !reloadBtn)
        {
            // If the reload button is no longer held,
            // we can now accept interrupt requests.
            _reloadSequenceState = RSS_InProgressInterruptible;
        }
        else if (_reloadSequenceState == RSS_InProgressInterruptible && reloadBtn)
        {
            // Reload button pressed again, interrupt current reloading sequence.
            _reloadSequenceState = RSS_Interrupted;
        }
        else if (_reloadSequenceState == RSS_Finished && !reloadBtn)
        {
            // Reload button released, exit from reloading sequence.
            _reloadSequenceState = RSS_None;
        }
    }

    //===========================================================================
    //
    // ReloadableWeapon::GetUpState
    //
    // Used to reset reload flag and timer.
    //
    //===========================================================================
    final override state GetUpState()
    {
        // Reset auto-reloading timer
        _autoReloadTimer = 0;
        // Reset reload sequence state
        _reloadSequenceState = RSS_None;

        PreRaise();
        return GetSelectState();
    }

    //===========================================================================
    //
    // ReloadableWeapon::PreRaise
    //
    // Called just before the weapon is raised.
    //
    //===========================================================================
    protected virtual void PreRaise()
    {
    }

    //===========================================================================
    //
    // ReloadableWeapon::GetSelectState
    //
    // Returns the select state.
    //
    //===========================================================================
    protected virtual state GetSelectState()
    {
        return Super.GetUpState();
    }

    //===========================================================================
    //
    // ReloadableWeapon::GetDownState
    //
    // Resets the player's "attack down" flag to ensure that +WEAPON.NOAUTOFIRE
    // keeps working for any other weapon we might switch to.
    //
    //===========================================================================
    final override state GetDownState()
    {
        if (Owner != null && Owner.player != null)
        {
            // Reset the "attack down" flag so that weapons
            // with +WEAPON.NOAUTOFIRE do not break.
            let plr = Owner.player;
            plr.attackdown = plr.cmd.buttons & (BT_ATTACK | BT_ALTATTACK);
        }

        PreLower();
        return GetDeselectState(IsInReloadSequence());
    }

    //===========================================================================
    //
    // ReloadableWeapon::PreLower
    //
    // Called just before the weapon is lowered.
    //
    //===========================================================================
    protected virtual void PreLower()
    {
    }

    //===========================================================================
    //
    // ReloadableWeapon::GetDeselectState
    //
    // Returns the deselect state.
    //
    //===========================================================================
    protected virtual state GetDeselectState(bool fromReload)
    {
        if (fromReload)
        {
            let deselectReloadState = ResolveState('DeselectReload');

            if (deselectReloadState != null)
            {
                return deselectReloadState;
            }
        }

        return Super.GetDownState();
    }

    //===========================================================================
    //
    // ReloadableWeapon::CheckAmmo
    //
    // Performs ammo-checking logic and selects the next attack state to send
    // the weapon to depending on whether there actually is enough ammo to shoot.
    //
    //===========================================================================
    final override bool CheckAmmo(int fireMode, bool autoSwitch, bool requireAmmo, int ammocount)
    {
        // Is this an alt-fire?
        bool altFire = fireMode == Weapon.AltFire;

        // Revert to default logic if checking for either fire
        // or if not using ammo for this fire mode.
        if (fireMode == Weapon.EitherFire || !UsesAmmo(altFire))
        {
            return Super.CheckAmmo(fireMode, autoSwitch, requireAmmo, ammocount);
        }

        // Are we using the clip for this fire mode?
        bool usesClip = UsesClip(altFire);
        // Do we have enough ammo in the clip for this fire mode?
        bool hasClip = CheckClip(fireMode, ammocount);
        // Do we have enough ammo for the corresponding fire mode?
        bool hasAmmo = HasEnoughAmmo(altFire, requireAmmo, ammocount);
        // Do we have ANY kind of ammo? (incl. clip)
        bool hasOtherModeAmmo = GetFireState(!altFire, FT_Normal) != null && HasEnoughAmmo(!altFire, requireAmmo);
        bool hasAnyAmmo = hasAmmo || hasOtherModeAmmo || CheckClip();

        // Normally, this is what we should return.
        bool result = hasAmmo || usesClip && hasClip;

        if (autoSwitch)
        {
            int attackBtn = altFire ? BT_ALTATTACK : BT_ATTACK;

            if (Owner != null && Owner.player != null && (Owner.player.cmd.buttons & attackBtn))
            {
                // This block is called from PlayerPawn.FireWeapon.

                // Can we fire the weapon?
                bool canFire = usesClip ? hasClip : hasAmmo;

                // Fire the weapon if we can.
                if (canFire)
                {
                    _nextAttackState = AS_NormalFire;
                    return true;
                }

                int refire = Owner.player.refire;

                // If not refiring and reloading mode is not manual,
                // check if we can reload now.
                if (!refire && GetReloadMode() != RM_MANUAL)
                {
                    // Can we reload the weapon?
                    if (usesClip && hasAmmo)
                    {
                        // If no ammo in clip and we are not re-firing,
                        // go to reload state.
                        _nextAttackState = AS_Reload;
                        return true;
                    }
                }

                // We can't (or won't) reload, so opt for dry fire instead.
                if (GetDryFireState(altFire, DFT_Normal))
                {
                    // If the last state was normal fire and we are re-firing,
                    // send to cooldown instead.
                    if (_nextAttackState == AS_NormalFire && refire)
                    {
                        _nextAttackState = AS_DryFireCooldown;
                    }
                    else
                    {
                        _nextAttackState = AS_DryFireOrHold;
                    }

                    return true;
                }

                // We shouldn't refire if we got here.
                Owner.player.refire = 0;

                // Do not fire the weapon.
                result = false;
            }

            // Fall through from above.
            // This block is also called from A_ReFire
            // when the player has released the fire button.

            // Auto-switch weapons if we don't have ammo
            // for any kind of attack.
            if (!hasAnyAmmo)
            {
                AutoSwitchWeapons();
                return false;
            }
        }

        //...and return the result.
        return result;
    }

    //===========================================================================
    //
    // ReloadableWeapon::CheckClip
    //
    // Checks whether there is enough ammo in the clip.
    // If ammocount is -1, uses the default ammo use value based on fire mode.
    //
    //===========================================================================
    protected bool CheckClip(int fireMode = Weapon.EitherFire, int ammocount = -1)
    {
        if (fireMode == Weapon.EitherFire)
        {
            return CheckClip(Weapon.PrimaryFire) || CheckClip(Weapon.AltFire);
        }

        bool altFire = fireMode == Weapon.AltFire;
        return UsesClip(altFire) && _clip.CheckAmmo(GetAmmoUse(altFire, ammocount));
    }

    //===========================================================================
    //
    // ReloadableWeapon::GetAmmoUse
    //
    // Returns the correct ammo usage value based on the provided value
    // and the alt-fire flag.
    //
    //===========================================================================
    private int GetAmmoUse(bool altFire, int ammocount)
    {
        // Default value -1 means "use value from clip", however,
        // since the clip only has one value, we should manually
        // fall back to AmmoUse2 if checking for alt fire.
        if (altFire && ammocount == -1)
        {
            ammocount = AmmoUse2;
        }

        return ammocount;
    }

    //===========================================================================
    //
    // ReloadableWeapon::UsesAmmo
    //
    // Checks if this weapon uses ammo for the corresponding fire mode.
    //
    //===========================================================================
    private bool UsesAmmo(bool altFire)
    {
        return !altFire || AmmoType2 != null && AmmoUse2 > 0;
    }

    //===========================================================================
    //
    // ReloadableWeapon::UsesClip
    //
    // Checks if this weapon uses its clip for the corresponding fire mode.
    //
    //===========================================================================
    private bool UsesClip(bool altFire)
    {
        return !altFire || UsesSameSecondaryAmmo();
    }

    //===========================================================================
    //
    // ReloadableWeapon::UsesSameSecondaryAmmo
    //
    // Checks if this weapon uses the same secondary ammo as the primary ammo.
    //
    //===========================================================================
    private bool UsesSameSecondaryAmmo()
    {
        return AmmoType2 == AmmoType1;
    }

    //===========================================================================
    //
    // ReloadableWeapon::HasEnoughAmmo
    //
    // Checks if there is enough ammo for the corresponding fire mode
    // by calling base implementation of CheckAmmo.
    //
    //===========================================================================
    private bool HasEnoughAmmo(bool altFire, bool requireAmmo = true, int ammocount = -1)
    {
        return !UsesAmmo(altFire) || Super.CheckAmmo(altFire ? Weapon.AltFire : Weapon.PrimaryFire, false, requireAmmo, ammocount);
    }

    //===========================================================================
    //
    // ReloadableWeapon::AutoSwitchWeapons
    //
    // Sets a new pending weapon to auto-switch weapons.
    //
    //===========================================================================
    private void AutoSwitchWeapons()
    {
        let pp = PlayerPawn(Owner);

        // Only auto-switch if the player doesn't already have a pending weapon
        if (pp != null && pp.player != null && pp.player.PendingWeapon == WP_NOCHANGE)
        {
            let bestWeapon = pp.BestWeapon(null);

            // Do not auto-switch if no weapon.
            if (bestWeapon != null)
            {
                pp.player.PendingWeapon = bestWeapon;
            }
        }
    }

    //===========================================================================
    //
    // ReloadableWeapon::DepleteAmmo
    //
    // Depletes ammo from the clip if it is used in this fire mode.
    // Otherwise, uses base implementation.
    //
    //===========================================================================
    final override bool DepleteAmmo(bool altFire, bool checkEnough, int ammouse)
    {
        // Deplete ammo from the clip if we use it in this fire mode.
        if (UsesClip(altFire))
        {
            return _clip.DepleteAmmo(GetAmmoUse(altFire, ammouse));
        }

        return Super.DepleteAmmo(altFire, checkEnough, ammouse);
    }

    //===========================================================================
    //
    // ReloadableWeapon::GetNextAttackState
    //
    // Decides which state to go to when the attack button is held.
    //
    //===========================================================================
    private state GetNextAttackState(bool altFire, bool hold)
    {
        // Check for normal fire.
        if (!UsesAmmo(altFire) || _nextAttackState == AS_NormalFire)
        {
            // Check if we are exiting a reload sequence.
            bool fromReload = IsInReloadSequence();

            // Force reset reload sequence state here
            // so that we can reload again if the reload button is held.
            _reloadSequenceState = RSS_None;

            // Pick a normal fire or hold state.
            return fromReload
                ? GetFireState(altFire, FT_FromReload)
                : GetFireState(altFire, hold ? FT_Hold : FT_Normal);
        }

        if (_nextAttackState == AS_DryFireOrHold)
        {
            //Dry fire.
            return GetDryFireState(altFire, hold ? DFT_Hold : DFT_Normal);
        }

        if (_nextAttackState == AS_DryFireCooldown)
        {
            //Dry fire cooldown.
            return GetDryFireState(altFire, DFT_Cooldown);
        }

        if (_nextAttackState == AS_Reload)
        {
            //Reload.
            return BeginReload();
        }

        // We should not get here.
        ThrowAbortException("%s: Encountered invalid value %d of _nextAttackState.", GetClassName(), _nextAttackState);
        return null;
    }

    //===========================================================================
    //
    // ReloadableWeapon::GetAtkState
    //
    // Propagates the call to GetNextAttackState.
    //
    //===========================================================================
    final override state GetAtkState(bool hold)
    {
        return GetNextAttackState(false, hold);
    }

    //===========================================================================
    //
    // ReloadableWeapon::GetAltAtkState
    //
    // Propagates the call to GetNextAttackState.
    //
    //===========================================================================
    final override state GetAltAtkState(bool hold)
    {
        return GetNextAttackState(true, hold);
    }

    //===========================================================================
    //
    // ReloadableWeapon::GetFireState
    //
    // Used instead of GetAtkState to retrieve fire state.
    //
    //===========================================================================
    protected virtual state GetFireState(bool altFire, EFireType type)
    {
        if (type == FT_FromReload)
        {
            let fireReloadState = altFire
                ? ResolveState('AltFireReload')
                : ResolveState('FireReload');

            if (fireReloadState != null)
            {
                return fireReloadState;
            }
        }

        bool hold = type == FT_Hold;
        return altFire
            ? Super.GetAltAtkState(hold)
            : Super.GetAtkState(hold);
    }

    //===========================================================================
    //
    // ReloadableWeapon::GetDryFireState
    //
    // Returns the dry-fire state for the corresponding fire mode and type.
    //
    //===========================================================================
    protected virtual state GetDryFireState(bool altFire, EDryFireType type)
    {
        if (type == DFT_Cooldown)
        {
            let cooldownState = altFire
                ? ResolveState('DryAltFireCooldown')
                : ResolveState('DryFireCooldown');

            if (cooldownState != null)
            {
                return cooldownState;
            }

            type = DFT_Hold;
        }

        if (type == DFT_Hold)
        {
            let holdState = altFire
                ? ResolveState('DryAltHold')
                : ResolveState('DryHold');

            if (holdState != null)
            {
                return holdState;
            }
        }

        return altFire
            ? ResolveState('DryAltFire')
            : ResolveState('DryFire');
    }

    //===========================================================================
    //
    // ReloadableWeapon::PreReload
    //
    // Called before reloading is about to begin.
    //
    //===========================================================================
    protected virtual void PreReload()
    {
    }

    //===========================================================================
    //
    // ReloadableWeapon::GetReloadState
    //
    // Returns the reload state.
    //
    //===========================================================================
    protected virtual state GetReloadState()
    {
        return ResolveState('Reload');
    }

    //===========================================================================
    //
    // ReloadableWeapon::GetReloadState
    //
    // Returns the state that concludes the reloading sequence.
    //
    //===========================================================================
    protected virtual state GetReloadDoneState()
    {
        return ResolveState('ReloadDone');
    }

    //===========================================================================
    //
    // ReloadableWeapon::CheckForReloadRequest
    //
    // Checks if the weapon should be reloaded right now.
    //
    //===========================================================================
    private bool CheckForReloadRequest()
    {
        // Deny all requests if we can't reload.
        if (!CanReload())
        {
            return false;
        }

        // Check for manual reload request (button).
        bool willReload = IsReloadButtonPressed();

        //Check for automatic reload request (timer).
        if (!willReload)
        {
            bool autoReload = false;

            if (Owner != null)
            {
                // Check for full auto mode.
                bool fullAuto = GetReloadMode() == RM_FULLAUTO;

                // To reload automatically, reload mode must be full auto
                // and there must be enough ammo to satisfy the minimum threshold.
                autoReload = fullAuto && CheckAutoReloadThreshold();
            }

            bool shouldReload = autoReload && !CheckClip();

            if (shouldReload)
            {
                // We will reload automatically if the timer has reached 0.
                willReload = _autoReloadTimer >= AUTORELOAD_DELAY;

                if (!willReload)
                {
                    _autoReloadTimer++;
                }
            }

            if (!shouldReload || willReload)
            {
                // Reset the timer if we will reload now
                // or if we can't reload at all.
                _autoReloadTimer = 0;
            }
        }

        return willReload;
    }

    //===========================================================================
    //
    // ReloadableWeapon::CheckAutoReloadThreshold
    //
    // Checks that the owner has enough ammo to start an automatic reload.
    // The exact amount is controlled by the user via the corresponding setting.
    //
    //===========================================================================
    private bool CheckAutoReloadThreshold()
    {
        if (IsInfiniteAmmo())
        {
            // Account for infinite ammo.
            // (since it's infinite, we always have enough!)
            return true;
        }

        // Get the current ammo amount, minus the part
        // that cannot be used for reloading.
        int curAmmoAmount = Ammo1.Amount - Ammo1.Amount % AmmoUse1;

        // Get the required threshold from the settings.
        int threshold = GetAutoReloadThreshold();

        // To reload automatically, the current value
        // must be no less than the threshold.
        return GetValueAsClipPercentage(curAmmoAmount) >= threshold;
    }

    //===========================================================================
    //
    // ReloadableWeapon::GetValueAsClipPercentage
    //
    // Converts the specified value to a percentage of the clip capacity.
    //
    //===========================================================================
    private double GetValueAsClipPercentage(int value) const
    {
        return double(value) / GetClipCapacity() * 100;
    }

    //===========================================================================
    //
    // ReloadableWeapon::IsReloadButtonPressed
    //
    // Returns true if the reload button is pressed.
    //
    //===========================================================================
    private bool IsReloadButtonPressed()
    {
        return Owner != null && Owner.player != null && (Owner.player.buttons & BT_RELOAD);
    }

    //===========================================================================
    //
    // ReloadableWeapon::IsInReloadSequence
    //
    // Returns true if this weapon is currently in a reload sequence
    // (round by round reloading).
    //
    //===========================================================================
    private bool IsInReloadSequence(bool checkFinished = false)
    {
        return _reloadSequenceState != RSS_None && (_reloadSequenceState != RSS_Finished || checkFinished);
    }

    //===========================================================================
    //
    // ReloadableWeapon::CanFireInReloadSequence
    //
    // Dry-fire is not supported in reloading sequences,
    // so check if we have enough ammo first.
    //
    //===========================================================================
    private bool, bool CanFireInReloadSequence()
    {
        // For alt-fire, check either the clip (if using it),
        // or normal ammo.
        bool canAltFire = UsesClip(Weapon.AltFire)
            ? CheckClip(Weapon.AltFire)
            : HasEnoughAmmo(true);

        return CheckClip(Weapon.PrimaryFire), canAltFire;
    }

    //===========================================================================
    //
    // ReloadableWeapon::IsInfiniteAmmo
    //
    // Checks if infinite ammo is enabled either via DMFlags or a powerup.
    //
    //===========================================================================
    protected bool IsInfiniteAmmo()
    {
        return sv_infiniteammo || Owner && Owner.FindInventory('PowerInfiniteAmmo', true);
    }

    //===========================================================================
    //
    // ReloadableWeapon::CanReload
    //
    // Checks if reloading is currently possible.
    //
    //===========================================================================
    private bool CanReload()
    {
        // Do not reload while dead
        if (Owner == null || Owner.health <= 0)
        {
            return false;
        }

        // We can always reload if the clip is not full and ammo is infinite OR optional.
        bool canAlwaysReload = (IsInfiniteAmmo() || bAmmo_Optional) && !_clip.IsFull();
        return Ammo1 != null && (canAlwaysReload || _clip.CanLoadAmmo(Ammo1.Amount));
    }

    //===========================================================================
    //
    // ReloadableWeapon::BeginReload
    //
    // Resets some player values related to weapon state, and begins reloading.
    //
    //===========================================================================
    private state BeginReload()
    {
        if (Owner == null || Owner.player == null)
        {
            return null;
        }

        let plr = Owner.player;

        // Disable mugshot rampage face while reloading
        plr.attackdown = false;
        // Reset the refire counter just in case.
        plr.refire = 0;

        PreReload();

        // Now go to the reload state.
        return GetReloadState();
    }

    //===========================================================================
    //
    // ReloadableWeapon::DoReload
    //
    // Performs the actual reload routine.
    //
    //===========================================================================
    private void DoReload(int maxAmt)
    {
        // Ideally, we want to load as much ammo as requested.
        int amt = maxAmt;
        // Check for infinite ammo.
        bool isInfiniteAmmo = IsInfiniteAmmo();

        // If we DON'T have infinite ammo nor optional ammo, cap the amount
        // of ammo to reload to the current reserve amount.
        if (!isInfiniteAmmo && !bAmmo_Optional)
        {
            amt = Min(amt, Ammo1.Amount);
        }

        // Load ammo into the clip.
        int loaded = _clip.LoadAmmo(amt);

        // Unless we have infinite ammo, we will subtract the amount of ammo loaded
        // from the current amount.
        if (!isInfiniteAmmo)
        {
            // Max is needed in case of optional ammo
            Ammo1.Amount = Max(Ammo1.Amount - loaded, 0);

            // If the new amount is 0 and the player has switched to another weapon
            // that uses the same ammo, cancel the switch.
            // IMO, this is a GZDoom bug, regardless of what the devs say,
            // I shouldn't be testing values like sv_dontcheckammo manually.
            if (!sv_dontcheckammo && Ammo1.Amount == 0 && Owner != null && Owner.player != null)
            {
                let pw = Owner.player.PendingWeapon;

                if (pw != null && pw != WP_NOCHANGE && !pw.CheckAmmo(EitherFire, false))
                {
                    Owner.player.PendingWeapon = WP_NOCHANGE;
                }
            }
        }
    }

    //===========================================================================
    //
    // ReloadableWeapon::ReloadFullClip
    //
    // Attempts to load as much ammo as possible into the clip,
    // up to its capacity.
    //
    //===========================================================================
    private void ReloadFullClip()
    {
        DoReload(_clip.GetCapacity());
    }

    //===========================================================================
    //
    // ReloadableWeapon::ReloadOneRound
    //
    // Reloads AmmoUse1 rounds into the clip.
    //
    //===========================================================================
    private void ReloadOneRound()
    {
        DoReload(AmmoUse1);

        // Mark the reload sequence state as in progress.
        // Now if the user releases the reload button and presses it again,
        // an interrupt request will be generated to terminate the sequence.
        if (_reloadSequenceState == RSS_None)
        {
            _reloadSequenceState = RSS_InProgress;
        }
    }

    //===========================================================================
    //
    // ReloadableWeapon::CheckReloadDone
    //
    // Checks if a reloading sequence has been interrupted
    // or if reloading is no longer possible.
    //
    //===========================================================================
    private bool CheckReloadDone()
    {
        bool interrupted = false;
        bool result = !CanReload() || (interrupted = _reloadSequenceState == RSS_Interrupted);

        if (result)
        {
            // If reloading sequence has been interrupted, set state to Finished
            // so that it doesn't restart right away.
            // Otherwise, it's safe to reset state straight to None,
            // since in this case we can only reload again if we find some ammo.
            _reloadSequenceState = interrupted ? RSS_Finished : RSS_None;
        }

        return result;
    }

    //===========================================================================
    //
    // ReloadableWeapon::GetClipCapacity
    //
    // Returns the clip capacity of this weapon.
    //
    //===========================================================================
    int GetClipCapacity() const
    {
        return _clip != null
            ? _clip.GetCapacity()
            : _clipCapacity;
    }

    //===========================================================================
    //
    // ReloadableWeapon::GetClipAmount
    //
    // Returns the amount of ammo in clip for this weapon.
    //
    //===========================================================================
    int GetClipAmount() const
    {
        return _clip.GetAmount();
    }

    //===========================================================================
    //
    // ReloadableWeapon::IsFullClip
    //
    // Returns true if the weapon's clip is full.
    //
    //===========================================================================
    bool IsFullClip() const
    {
        return _clip.IsFull();
    }

    //===========================================================================
    //
    // ReloadableWeapon::GetClipAmountPercentage
    //
    // Returns the amount of ammo in clip as a percentage of its capacity.
    //
    //===========================================================================
    double GetClipAmountPercentage() const
    {
        return GetValueAsClipPercentage(GetClipAmount());
    }

    //===========================================================================
    //
    // ReloadableWeapon::SetClipAmmoGive
    //
    // Normally, the weapon's clip will be loaded with as much ammo as possible
    // when picking it up for the first time.
    // This method overrides this behavior and sets a fixed amount to be loaded
    // into the weapon's clip when it is first picked up.
    // This could be used to properly transfer the clip amount
    // when a weapon is created upon a player's death.
    // Right now there is no function to transfer old weapon's properties
    // to the new one, but maybe one day it becomes possible.
    //
    //===========================================================================
    private void SetClipAmmoGive(int clipAmount)
    {
        _clipAmmoGive = clipAmount;
    }

    //===========================================================================
    //
    // ReloadableWeapon::BeginPlay
    //
    // Initializes the clip.
    //
    //===========================================================================
    override void BeginPlay()
    {
        Super.BeginPlay();

        // These flags are not supported for reloadable weapons,
        // in case they're set, make sure to abort now.
        if (bPrimary_Uses_Both || bAlt_Uses_Both)
        {
            ThrowAbortException("%s: Reloadable weapons do not support PRIMARY_USES_BOTH and ALT_USES_BOTH flags.", GetClassName());
        }

        // Make sure the weapon uses an ammo type certified for use
        // with reloadable weapons. This is only needed to fix auto-switch issues,
        // everything else works fine without it. Just enforcing consistency.
        if (!(AmmoType1 is 'ReloadableAmmo'))
        {
            ThrowAbortException("%s: Primary ammo type must descend from ReloadableAmmo.", GetClassName());
        }

        // Create the clip.
        _clip = WeaponClip.Create(_clipCapacity, AmmoUse1);
        // Initially, the entire clip will be loaded into the weapon
        // when it is picked up (if AmmoGive amounts allow this),
        // this can be overridden by calling SetClipAmmoGive right after spawning.
        _clipAmmoGive = _clip.GetCapacity();
    }

    //===========================================================================
    //
    // ReloadableWeapon::PostBeginPlay
    //
    // Loads the clip if the weapon is part of initial inventory.
    //
    //===========================================================================
    override void PostBeginPlay()
    {
        if (Owner != null && Ammo1 != null)
        {
            // Check if this weapon is part of newly-given default inventory.
            // In this case, we want the clip to be initially loaded.
            // This is possible in two cases:
            // 1) Owner has just spawned (new game, multiplayer respawn)
            // 2) Owner has moved to another level via ChangeLevel
            //    with a flag to reset their inventory.
            //
            // NB: 2) will not work in GZDoom 4.4.2 and below.
            // This is a GZDoom bug that was fixed in newer versions.
            if (Owner.GetAge() == 0 || Level.maptime == 0)
            {
                // Load as much ammo as possible into the weapon's clip.
                Ammo1.Amount -= _clip.LoadAmmo(Ammo1.Amount);
            }
        }

        Super.PostBeginPlay();
    }

    //===========================================================================
    //
    // ReloadableWeapon::CreateCopy
    //
    // Handles proper copying of the weapon's clip.
    //
    //===========================================================================
    override Inventory CreateCopy(Actor other)
    {
        let copy = ReloadableWeapon(Super.CreateCopy(other));

        if (copy != null && copy != self)
        {
            // Normally, this will not change anything,
            // since the only time when a "real" copy is required
            // is when this weapon stays, and if it does,
            // then its clip ammo give value will be the default one.
            // However, logically, this is what should be done.
            // And it is possible that the clip ammo give parameter
            // will be exposed to mappers in the future.
            copy.SetClipAmmoGive(_clipAmmoGive);

            // And make sure the ammo in the clip is not lost, too.
            // Again, usually this is not needed,
            // but we want to make sure everything works 100% right,
            // even if it's a niche case.
            copy._clip.LoadAmmo(_clip.GetAmount());
        }

        return copy;
    }

    //===========================================================================
    //
    // ReloadableWeapon::CreateTossable
    //
    // Handles proper assignment of the weapon's properties
    // when dropping the weapon from inventory.
    //
    //===========================================================================
    override Inventory CreateTossable(int amount)
    {
        let tossable = ReloadableWeapon(Super.CreateTossable(amount));

        if (tossable != null)
        {
            // Not sure why this isn't the default for dropped weapons.
            tossable.bIgnoreSkill = true;

            // Make sure the new owner only gets this much ammo in the clip,
            // even if AmmoGive values are changed later.
            tossable.SetClipAmmoGive(_clip.GetAmount());
        }

        return tossable;
    }

    //===========================================================================
    //
    // ReloadableWeapon::HandlePickup
    //
    // Handles the pickup of another weapon of the same type.
    //
    //===========================================================================
    override bool HandlePickup(Inventory item)
    {
        if (item.GetClass() == GetClass())
        {
            // If picking up an extra weapon, make sure to unload its clip first.
            // This way, ammo from the clip will appear in owner's inventory.
            // Unfortunately, it is not possible to do this before actually
            // realizing if the weapon can be picked up or not.
            // This is because Weapon.PickupForAmmo is not overridable.
            // That's why we store the clip ammo give value separately
            // to transfer the ammo back into the clip
            // if someone else tries to pick it up.
            let weaponOnGround = ReloadableWeapon(item);
            weaponOnGround.AmmoGive1 += weaponOnGround._clip.UnloadAmmo();
        }

        // Make sure we will not wrongly auto-switch to another weapon.
        // See the corresponding mixin (HandlePickupHook) for details.
        return SpecialHandlePickup(item);
    }

    //===========================================================================
    //
    // ReloadableWeapon::AttachToOwner
    //
    // Loads ammo into the clip before picking this weapon up for the first time.
    //
    //===========================================================================
    override void AttachToOwner(Actor other)
    {
        int prevAmount, prevMax;

        // Make room for extra clip ammo in the owner's inventory.
        //
        // OK, this is a small hack, but it's needed for skill ammo factors
        // to work properly. The problem is that all the multiplications happen
        // in AttachToOwner which calls AddAmmo, and we cannot override it.
        // AddAmmo works by (obviously) using the AmmoGive values, and it cannot
        // take the weapon's clip into account, since it doesn't know about it.
        // If we move ammo to the clip before AddAmmo is called, then AddAmmo
        // will have wrong values to work with.
        //
        // So we have to let all ammo to be given normally first, and only then
        // we transfer it to the clip. But since the clip confers extra capacity,
        // we have to make room for it by temporarily boosting the MaxAmount value
        // of the ammo itself.

        // Find the ammo item, or add it if it doesn't exist yet.
        let ammoitem = Ammo(other.FindInventory(AmmoType1));

        if (ammoitem == null)
        {
            ammoitem = Ammo(Spawn(AmmoType1));

            if (ammoitem)
            {
                ammoitem.Amount = 0; // !!!
                ammoitem.AttachToOwner(other);
            }
        }

        if (ammoitem != null)
        {
            if (!sv_unlimited_pickup)
            {
                // Remember the current MaxAmount to restore it later.
                prevMax = ammoitem.MaxAmount;

                // Temporarily boost its MaxAmount to accommodate for the clip ammo.
                // NB: if we're somehow already beyond the max amount,
                // make sure to account for it, or it can lead to loss of ammo!
                ammoitem.MaxAmount = Max(prevMax, ammoitem.Amount) + _clip.GetCapacity();
            }

            // Remember the current amount.
            prevAmount = ammoitem.Amount;

            // Unload all ammo from the clip.
            AmmoGive1 += _clip.UnloadAmmo();
        }

        // The weapon is picked up and ammo is given...
        Super.AttachToOwner(other);

        if (ammoitem != null)
        {
            // Load at most clip ammo give ammo to the clip, but do not deduct
            // from the ammo the owner had before picking up the weapon.
            int maxLoad = Min(_clipAmmoGive, ammoitem.Amount - prevAmount);

            // Transfer remaining ammo to the clip.
            ammoitem.Amount -= _clip.LoadAmmo(maxLoad);

            if (!sv_unlimited_pickup)
            {
                // Now remove the extra room we made earlier by resetting MaxAmount
                // to the value remembered initially.
                ammoitem.MaxAmount = prevMax;

                // Make sure we haven't ended up with extra ammo beyond the max amount.
                // This can happen if picking up a dropped weapon.
                // NB: if we were somehow already beyond the max amount,
                // make sure to account for it, or it can lead to loss of ammo!
                ammoitem.Amount = Min(ammoitem.Amount, Max(prevAmount, ammoitem.MaxAmount));
            }
        }
    }

    //===========================================================================
    //
    // ReloadableWeapon::OwnerDied
    //
    // Special handling to drop the weapon after death in the proper way.
    // The proper way doesn't involve creating a copy of the weapon.
    // This way, all actions performed when the weapon is dropped normally
    // are processed.
    //
    //===========================================================================
    override void OwnerDied()
    {
        Super.OwnerDied();

        // Check if weapon dropping is enabled and the weapon is active.
        if (!sv_weapondrop || Owner == null || Owner.player == null || Owner.player.ReadyWeapon != self)
        {
            return;
        }

        // Owner will likely be nulled after the weapon has been dropped,
        // so we have to remember it here.
        let prevOwner = Owner;

        // Try to drop the weapon.
        let dropped = Weapon(Owner.DropInventory(self));

        if (dropped == null)
        {
            // Whoops.
            return;
        }

        // All ammo this weapon uses is transferred to the weapon.
        if (AmmoType1 != null)
        {
            let ammoitem = prevOwner.FindInventory(AmmoType1);

            if (ammoitem != null)
            {
                AmmoGive1 += ammoitem.Amount;
                ammoitem.Amount = 0;
            }
        }

        // Same for secondary ammo, if it is used.
        if (AmmoType2 != null)
        {
            let ammoitem = prevOwner.FindInventory(AmmoType2);

            if (ammoitem != null)
            {
                AmmoGive2 += ammoitem.Amount;
                ammoitem.Amount = 0;
            }
        }

        // Prevent other weapons from being dropped by clearing ReadyWeapon
        // (either via our logic or normal sv_weapondrop handling).
        prevOwner.player.ReadyWeapon = null;
    }

    //===========================================================================
    //
    // ReloadableWeapon::Travelled
    //
    // Called when the weapon has been moved to another level.
    //
    //===========================================================================
    override void Travelled()
    {
        Super.Travelled();

        if (Owner != null && Owner.player != null && Owner.player.ReadyWeapon == self)
        {
            // NB: the weapon is not always guaranteed to be raised again
            // on level change (e.g. if CHANGELEVEL_PRERAISEWEAPON flag is used),
            // so we have to make sure that PreRaise is still called.
            PreRaise();
        }
    }
}
