//===========================================================================
//
// HandlePickupHook
//
// Injected into HandlePickup overrides of reloadable weapons
// and ammo classes.
// See detailed explanation below.
//
//===========================================================================
mixin class HandlePickupHook
{
    //===========================================================================
    //
    // HandlePickupHook::SpecialHandlePickup
    //
    // This is a kind of a hack. I'm not feeling too proud after implementing it.
    //
    // The problem: imagine we are out of some kind of ammo, and we have a weapon
    // that uses this ammo and has some ammo loaded in its clip. When picking up
    // ammo, the HandlePickup method doesn't know anything about the clip, so it
    // will blindly attempt to initiate a weapon switch if the old amount was 0.
    // This is incorrect - the weapon shouldn't actually be auto-switched to,
    // since it isn't really "out of ammo".
    //
    // Thus, we have to monitor for this specific case and prevent it from
    // happening.
    //
    // This functionality is used in both ReloadableAmmo (base ammo class)
    // and ReloadableWeapon, in HandlePickup overrides.
    //
    //===========================================================================
    protected bool SpecialHandlePickup(Inventory item)
    {
        PlayerInfo plr = null;
        bool hasPendingWeapon;

        if (Owner != null && Owner.player != null)
        {
            plr = Owner.player;
            // Remember if we have a pending weapon now.
            hasPendingWeapon = plr.PendingWeapon != WP_NOCHANGE;
        }

        // Try to handle the pickup.
        bool result = Super.HandlePickup(item);

        // If the player didn't have a pending weapon before...
        if (result && plr != null && !hasPendingWeapon)
        {
            // ...check if it's reloadable and non-empty.
            let newPendingWeapon = ReloadableWeapon(plr.PendingWeapon);

            if (newPendingWeapon != null && newPendingWeapon.GetClipAmount() > 0)
            {
                // If that is true, cancel the switch.
                plr.PendingWeapon = WP_NOCHANGE;
            }
        }

        return result;
    }
}
