//===========================================================================
//
// ReloadableAmmo
//
// Base class for ammo types used by reloadable weapons.
//
//===========================================================================
class ReloadableAmmo : Ammo abstract
{
    // Enable the SpecialHandlePickup method to prevent auto-switching
    // to a non-empty weapon.
    mixin HandlePickupHook;

    //===========================================================================
    //
    // ReloadableAmmo::GetParentAmmo
    //
    // Determines the parent ammo class for this ammo.
    //
    //===========================================================================
    override class<Ammo> GetParentAmmo()
    {
        class<Object> cls = GetClass();

        while (cls != null && cls.GetParentClass() != 'ReloadableAmmo')
        {
            cls = cls.GetParentClass();
        }

        return (class<Ammo>)(cls);
    }

    //===========================================================================
    //
    // ReloadableAmmo::HandlePickup
    //
    // Peforms an additional check related to automatic weapon switching.
    // See the corresponding mixin (HandlePickupHook) for details.
    //
    //===========================================================================
    override bool HandlePickup(Inventory item)
    {
        return SpecialHandlePickup(item);
    }
}
