version "4.3"

#include "classes/core/ReloadableWeapon.zs"
#include "classes/core/ReloadableAmmo.zs"
#include "classes/core/WeaponClip.zs"
#include "classes/core/HandlePickupHook.zs"

#include "classes/core/sbarinfo_support/SbarinfoSupportPlayer.zs"
#include "classes/core/sbarinfo_support/SbarinfoSupportItem.zs"
#include "classes/core/sbarinfo_support/MagazineCheck.zs"
#include "classes/core/sbarinfo_support/MagazineCounter.zs"

#include "classes/examples/ExampleHud.zs"
